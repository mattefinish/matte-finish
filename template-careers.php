<?php 
/*
 * Template name: Careers
 */

include_once 'includes/get-jobs.php';



get_header();

?>

	<div class="content-wrap scroll-point">

		<div class="container container-career">

			<div class="page_header">
				<h1>Careers</h1>
			</div>

			<?php
			foreach($jobs as $job):
			?>	
				<div class="career_card">
					<div class="career_title">
						<h1><?= $job->title ?> <span class="career_type">(<?=$job->employment_type?>)</span></h1>
					</div>
					<div class="career_description">

						<div class="career_description_short">
								<?= $job->content_short ?>
						</div>
						
						<?php if ( $job->content_long ):?>
						<div class="more-info-job-wrapper">
							<span class="more-info-job-link" id="open-more-info-<?=$job->id?>" style="text-decoration:underline" onclick="openMoreInfo(<?=$job->id?>)">Show more</span>
							<div class="more-info-job" class="career_description_long" id="more-info-<?=$job->id?>">
								<?= $job->content_long ?>
							</div>
							<span class="more-info-job more-info-job-link"  id="close-more-info-<?=$job->id?>" style="text-decoration:underline" onclick="closeMoreInfo(<?=$job->id?>)">Show less</span>
						</div>
						<?php endif; ?>

						
						
						<a href="<?= $job->url ?>" target="_blank" class="matte_button">Apply</a>
					</div>
				</div>
			<?php endforeach; ?>

			<script>

				function openMoreInfo(id){
					$('#open-more-info-'+id).hide();
					$('#close-more-info-'+id).show();
					$('#more-info-'+id).slideDown(500);
				}

				function closeMoreInfo(id){
					$('#more-info-'+id).slideUp(400,function(){
						$('#close-more-info-'+id).hide();
						$('#open-more-info-'+id).show();
					});
					
				}
			</script>

			<style>
				.more-info-job-wrapper {
					padding-bottom:30px;
				}
				.more-info-job-link {
					cursor:pointer;
				}
				.more-info-job{
					display:none;
				}
				.container-career {
						max-width: 850px;
					}
				.career_title {
   				 	width: 40%;
				}
				.career_description {
   				 	width: 60%;
				}
				@media (max-width: 768px) {
					.career_title,
					.career_description {
						width: 100%;
					}
					.container-career {
						max-width: 550px;
					}
				}
				.career_description hr {
						margin-bottom:20px;
						margin-top:8px;
				}
				.career_description .matte_button {
					/* margin-top:8px; */
				}
			</style>

		</div>

	</div>

 
<?php get_footer(); ?>