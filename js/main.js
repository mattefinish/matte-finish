var $ = jQuery.noConflict();

$(document).ready(function() {
	/*========================
	Hero Slider
	========================*/

	var heroSlider = document.querySelector('.hero-slider');
	var heroSliderLinks = document.querySelectorAll('.hero-slider-nav__link');
	var heroSliderSlides = document.querySelectorAll('.hero-slide');

	// Activate Hero Slide

	function activateHeroSlide(sliderContainer, index) {
		var slides = sliderContainer.querySelectorAll('.hero-slide');
		var activeSlideClass = 'hero-slide--is-selected';
		// Hide active slide
		var activeSlide = sliderContainer.querySelector('.' + activeSlideClass);
		activeSlide.classList.remove(activeSlideClass);
		// Show new active slide
		slides[index].classList.add(activeSlideClass);

		var links = sliderContainer.querySelectorAll('.hero-slider-nav__link');
		var activeLinkClass = 'hero-slider-nav__link--is-selected';
		// Hide active link
		var activeLink = sliderContainer.querySelector('.' + activeLinkClass);
		activeLink.classList.remove(activeLinkClass);
		// Show new active link
		links[index].classList.add(activeLinkClass);
	}

	// Event Listeners

	Array.from(heroSliderLinks).forEach((link, index) => {
		var sliderContainer = link.closest('.hero-slider-container');
		link.addEventListener('mouseenter', function() {
			activateHeroSlide(sliderContainer, index);
		});
		link.addEventListener('click', function() {
			activateHeroSlide(sliderContainer, index);
		});
	});

	// Lazyload Hidden Slides

	window.setTimeout(() => {
		heroSlider.classList.add('hero-slider--is-visible');
	}, 1000);

	/*======================== 
  PLYR INITIALIZATION
  ========================*/

	var players = plyr.setup('.matte-plyr', {
		autoplay: true,
		controls: ['play', 'progress', 'mute', 'volume'],
		tooltips: { controls: false, seek: false },
	});

	// Set player to mute if already playing on page load.
	// Plyr has a js issue where the volume can't be set on mobile devices.

	if (players) {
		if (!players[0].isPaused()) {
			players[0].setVolume(0);
		}
	}

	/*======================== 
  MOBILE NAVIGATION
  ========================*/

	/*=== Open the fullscreen overlay ===*/

	$('.menu_icon').click(function() {
		$('.mobile_navigation').fadeIn('fast');
	});

	/*=== Clicking the x within the overlay closes it ===*/

	$('.close_menu_icon').click(function() {
		$('.mobile_navigation').fadeOut('fast');
	});

	/*=================
  SHOW SEARCH & BACK TO TOP
  =================*/

	$(window).scroll(function() {
		if ($('.scroll-point').length) {
			var scroll = $(window).scrollTop();

			var navOffsetTop = $('.scroll-point').offset().top;

			if (scroll > navOffsetTop) {
				$('#back-to-top, #searchform-trigger').addClass('visible');
			} else {
				$('#back-to-top, #searchform-trigger').removeClass('visible');
			}
		}
	});

	/*=================
  MASONRY NEWS FEED
  =================*/

	/*=== On click of the more button change the text to 'loading' ===*/

	$('.alm-load-more-btn').on('click tap', function() {
		$(this).text('loading');
	});

	/*=== Ajax Load More callback function. Run after each successful AJLM query ===*/

	window.almComplete = function(alm) {
		// If newsfeed masonry grid

		if (alm.theme_repeater === 'alm-newsfeed-grid.php') {
			var $container = $('.newsfeed-grid ul');

			$container.imagesLoaded(function() {
				$('.news-item').fadeTo(1300, 1);
			});

			/*===  Once the grid is done loading then reset the text to 'more' ===*/

			$('.alm-load-more-btn').text('more');
		}

		// Else if the expand view
		else if (alm.theme_repeater === 'alm-newsfeed-expand.php') {
			// Find all youtube or vimeo iFrame elements

			var mediaFrames = $(
				'iframe[src*="youtube.com"], iframe[src*="vimeo.com"]'
			);

			// Iterate over each iFrame to check if it should add the video_embed wrapper

			mediaFrames.each(function() {
				if ($(this).parent('.fluid-width-video-wrapper').length == 1) {
				} else {
					$(this).wrap("<div class='video_embed'/>");
				}
			});

			/*=== Target div for fitVids ===*/

			$('.video_embed').fitVids();

			/*=== Once the grid is done loading then reset the text to 'more' ===*/

			$('.alm-load-more-btn').text('more');
		}
	};

	/*=== AJAX Load More callback when it is done loading posts ===*/

	almDone = function(alm) {
		// If newsfeed masonry grid

		if (alm.theme_repeater === 'alm-newsfeed-grid.php') {
			$('.alm-load-more-btn').text('more');
		}

		// Else if the expand view
		else if (alm.theme_repeater === 'alm-newsfeed-expand.php') {
			$('.alm-load-more-btn').text('more');
		}
	};

	/*================================= 
  BACK TO TOP
  =================================*/

	$('#back-to-top').click(function(e) {
		$('html, body').animate(
			{
				scrollTop: 0,
			},
			800
		);
		e.preventDefault();
	});

	/*================================= 
  REEL CUSTOM CONTROLS
  =================================*/

	if ($('.video-wrap').length !== 0) {
		/*=== Variables ===*/

		var video = document.getElementById('matte-reel');
		var videoControls = $('#video-controls');
		var playButton = document.getElementById('play-pause');
		var replayButton = document.getElementById('replay');
		var fullScreenButton = document.getElementById('full-screen');

		/*=== Pause Video by clicking on the video container ===*/

		$('.video-wrap').click(function() {
			if (video.paused == true) {
				videoControls.removeClass('visible');
				video.play();
			} else {
				videoControls.addClass('visible');
				video.pause();
			}
		});

		/*=== Play ===*/

		playButton.addEventListener('click', function() {
			video.play();
			videoControls.removeClass('visible');
		});

		/*=== Replay ===*/

		replayButton.addEventListener('click', function() {
			video.currentTime = 0;
			video.play();
			videoControls.removeClass('visible');
		});

		/*=== Fullscreen ===*/

		fullScreenButton.addEventListener('click', function() {
			if (video.requestFullscreen) {
				video.requestFullscreen();
			} else if (video.mozRequestFullScreen) {
				video.mozRequestFullScreen(); // Firefox
			} else if (video.webkitRequestFullscreen) {
				video.webkitRequestFullscreen(); // Chrome and Safari
			}
			video.play();
			videoControls.removeClass('visible');
		});
	}

	/*=================
  HEADER COLOR ON ARCHIVE PAGES
  =================*/

	function headerColorArchives() {
		$(window).scroll(function() {
			var navOffsetTop = $('.scroll-point').offset().top;
			var scrollTop = $(window).scrollTop();
			if (scrollTop > navOffsetTop) {
				$('#header').removeClass('white');
				$('#header').addClass('black');
				$('#header').addClass('background');
			}
		});
	}

	if (
		$('body').hasClass('archive') ||
		$('body').hasClass('page-template-template-careers')
	) {
		headerColorArchives();
	}

	/*=================
  HEADER REVEAL
  =================*/

	function connectReveal() {
		// show logo on hover

		$('#logo').hover(function() {
			$('#header #films-music').toggleClass('visible');
		});

		// show social icons in header on hover

		var hoverIn = function() {
			$(this).addClass('active');
		};

		var hoverOut = function() {
			$(this).removeClass('active');
		};

		$('#connect').hover(hoverIn, hoverOut);
	}

	connectReveal();
});

function iframeResponsive() {
	function setAspectRatio() {
		$('iframe').each(function() {
			$(this).css('height', ($(this).width() * 9) / 16);
		});
	}
	setAspectRatio();
	$(window).resize(setAspectRatio);
}

function headerColor() {
	var navOffsetTop = $('.scroll-point').offset().top;
	function colorChange() {
		var scrollTop = $(window).scrollTop();
		if (scrollTop > navOffsetTop - 100) {
			$('#header').removeClass('white');
			$('#header').addClass('black');
			$('#header').addClass('background');
		} else {
			$('#header').addClass('white');
			$('#header').removeClass('black');
			$('#header').removeClass('background');
		}
	}
	colorChange();
	$(window).scroll(function() {
		colorChange();
	});
}

// auto scroll
function autoScroll() {
	$('a.scroll-down').click(function(e) {
		e.preventDefault();
		var t = $(this).attr('href');
		$('html, body').animate(
			{
				scrollTop: $(t).offset().top - 70,
			},
			500
		);
	});
}

function hoverFilter() {
	$('.filter.fade-toggle').on('mouseover', function() {
		var targetPost = $(this).attr('data-id');
		$('.filtered-item.fade-toggle').each(function() {
			if ($(this).attr('id') === targetPost) {
				$(this).addClass('full-opacity');
			} else {
				$(this).removeClass('full-opacity');
			}
		});
		if ($('body').hasClass('home') || $('body').hasClass('page')) {
			if ($('#header').hasClass('black')) {
				$('#header').removeClass('black');
				$('#header').addClass('white');
			}
		}
		// Hide recent work button
		$('.recent-work').fadeOut('fast');
	});
	$('.filter.fade-toggle').on('mouseout', function() {
		$('.filtered-item.fade-toggle').removeClass('full-opacity');
		if ($('body').hasClass('home')) {
			if ($('#header').hasClass('white')) {
				$('#header').removeClass('white');
				$('#header').addClass('black');
			}
		}
		// Show recent work button
		$('.recent-work').fadeIn('fast');
	});
}

function slideToggleFilter() {
	$('#closeClients').on('click', function() {
		if ($('.scroll-point').length) {
			var navOffsetTop = $('.scroll-point').offset().top,
				scrollTop = $(window).scrollTop();

			if (scrollTop > navOffsetTop) {
				$('#header').addClass('background');
				$('#header').removeClass('white');
				$('#header').addClass('black');
			}
		}

		$('.filtered-item.slide-toggle').removeClass('down');

		if (
			$('body').hasClass('archive') ||
			($('body').hasClass('page') && !$('body').hasClass('home'))
		) {
			if ($('#header').hasClass('white')) {
				$('#header').removeClass('white');
				$('#header').addClass('black');
			}
		}

		$('.filter.slide-toggle[data-id="post-9"]').removeClass('active');

		$('.filter.slide-toggle[data-id="post-7"]').removeClass('click');
	});

	$('.filter.slide-toggle[data-id="post-9"]').on('click', function() {
		if ($(this).hasClass('active')) {
			// Only do the conditional scroll point code if the scroll point div is on the page.

			if ($('.scroll-point').length) {
				var navOffsetTop = $('.scroll-point').offset().top,
					scrollTop = $(window).scrollTop();

				if (scrollTop > navOffsetTop) {
					$('#header').addClass('background');
					$('#header').removeClass('white');
					$('#header').addClass('black');
				}
			}

			$('.filtered-item.slide-toggle').removeClass('down');

			if (
				$('body').hasClass('archive') ||
				($('body').hasClass('page') && !$('body').hasClass('home'))
			) {
				if ($('#header').hasClass('white')) {
					$('#header').removeClass('white');
					$('#header').addClass('black');
				}
			}

			$(this).removeClass('active');

			$('.filter.slide-toggle[data-id="post-7"]').removeClass('click');
		} else {
			var targetPost = $(this).attr('data-id');
			$('.filtered-item.slide-toggle').each(function() {
				if ($(this).attr('id') === targetPost) {
					if ($('#header').hasClass('background')) {
						$('#header').removeClass('background');
					}
					$(this).addClass('down');
				} else {
					$(this).removeClass('down');
				}
			});
			if ($('#header').hasClass('black')) {
				$('#header').removeClass('black');
				$('#header').addClass('white');
			}

			$(this).addClass('active');

			$('.filter.slide-toggle[data-id="post-7"]').addClass('click');
		}
	});
	$('.filter.slide-toggle:not([data-id="post-9"])').on('mouseover', function() {
		var targetPost = $(this).attr('data-id');
		$('.filtered-item.slide-toggle').each(function() {
			if ($(this).attr('id') === targetPost) {
				if ($('#header').hasClass('background')) {
					$('#header').removeClass('background');
				}
				$(this).addClass('down');
			} else {
				$(this).removeClass('down');
			}
		});
		if ($('#header').hasClass('black')) {
			$('#header').removeClass('black');
			$('#header').addClass('white');
		}
	});
	$('.filter.slide-toggle:not([data-id="post-9"])').on('mouseout', function() {
		// Only do the conditional scroll point code if the scroll point div is on the page.

		if ($('.scroll-point').length) {
			var navOffsetTop = $('.scroll-point').offset().top,
				scrollTop = $(window).scrollTop();

			if (scrollTop > navOffsetTop) {
				$('#header').addClass('background');
				$('#header').removeClass('white');
				$('#header').addClass('black');
			}
		}

		$('.filtered-item.slide-toggle').removeClass('down');

		if (
			$('body').hasClass('archive') ||
			($('body').hasClass('page') && !$('body').hasClass('home'))
		) {
			if ($('#header').hasClass('white')) {
				$('#header').removeClass('white');
				$('#header').addClass('black');
			}
		}
	});
}

function newsfeedToggle() {
	$('#expand').on('click', function() {
		$('.newsfeed-grid, #expand, .grid-read-more').hide();
		$('.newsfeed-expand, #collapse, .expand-read-more').show();
		$('.expand-read-more').css('display', 'block');
	});
	$('#collapse').on('click', function() {
		$('.newsfeed-grid, #expand, .expand-read-more').show();
		$('.newsfeed-expand, #collapse, .grid-read-more').hide();
	});
}

function bottomAlignmentRight() {
	var bottomRight = $('.bottom-right'),
		bottomRightPanel = $('.bottom-right')
			.parent()
			.parent(),
		bottomRightRow = $('.bottom-right')
			.parent()
			.parent()
			.parent();

	bottomRightRow.css('position', 'relative');
	bottomRightPanel.addClass('bottom-right-panel');
	bottomRightPanel.each(function() {
		var rowPadding = $(this)
			.parent()
			.css('padding');
		$(this).css('padding-right', rowPadding);
		$(this).css('padding-bottom', rowPadding);
	});
}

function bottomAlignmentLeft() {
	var bottomLeft = $('.bottom-left'),
		bottomLeftPanel = $('.bottom-left')
			.parent()
			.parent(),
		bottomLeftRow = $('.bottom-left')
			.parent()
			.parent()
			.parent();

	bottomLeftRow.css('position', 'relative');
	bottomLeftPanel.addClass('bottom-left-panel');
	bottomLeftPanel.each(function() {
		var rowPadding = $(this)
				.parent()
				.css('padding'),
			siblingPanel = $(this).siblings();

		siblingPanel.css('float', 'right');
		$(this).css('padding-left', rowPadding);
		$(this).css('padding-bottom', rowPadding);
	});
}

function moduleClose() {
	// close on window click if negative UI space exists
	$('.module').on('click', function(e) {
		if (e.target.className !== 'module-content') {
			$('.module').removeClass('visible');
			$('body').removeClass('no-scroll');
		}
	});
	// close on close button click
	$('.close').on('click', function() {
		console.log('hi');
		$('.module').removeClass('visible');
		$('body').removeClass('no-scroll');
	});
	// close on escape key event
	$('body').on('keydown', function(e) {
		if ((e.keyCode || e.which) == 27) {
			$('.module').removeClass('visible');
			$('body').removeClass('no-scroll');
		}
	});
}

function lightboxInit() {
	var lightboxItem = $('.lightbox-item').find('img');
	lightboxItem.on('click', function() {
		var imgUrl = $(this).attr('src'),
			lightbox = $('#lightbox');

		if (lightbox.length > 0) {
			$('#lightbox img').attr('src', imgUrl);
			$(lightbox).addClass('visible');
			$('body').addClass('no-scroll');
			moduleClose();
		} else {
			$('body').append(
				'<div id="lightbox" class="module visible"><img class="module-content" src="' +
					imgUrl +
					'"><button class="close">close</button></div>'
			);
			$(lightbox).addClass('visible');
			$('body').addClass('no-scroll');
			moduleClose();
		}
	});
}

function searchformInit() {
	$('#searchform-trigger').on('click', function() {
		$('.search-background').addClass('visible');
		$('body').addClass('no-scroll');
	});
	$('.close').on('click', function() {
		$('.search-background').removeClass('visible');
		$('body').removeClass('no-scroll');
	});
	$('body').on('keydown', function(e) {
		if ((e.keyCode || e.which) == 27) {
			$('.search-background').removeClass('visible');
			$('body').removeClass('no-scroll');
		}
	});
}

function mobileNav() {
	$('#mobile-trigger').on('click', function() {
		console.log('wut');
		$('#mobile-nav').toggle();
	});
}

function searchPlaceholder() {
	$('input#s').attr('placeholder', 'search');
}

function readmoreGrid() {
	var posts = $('.newsfeed-grid .news-item');
	posts.slice(12).hide();
	if (posts.length > 5) {
		$('.grid-read-more').on('click', function() {
			console.log('hi');
			$('.newsfeed-grid .news-item:hidden')
				.slice(0, 6)
				.show();
			if ($('.newsfeed-grid .news-item:hidden').length === 0) {
				$('.read-more').fadeOut(50);
			}
		});
	}
}

function readmoreExpand() {
	var posts = $('.newsfeed-expand .news-item');
	posts.slice(6).hide();
	if (posts.length > 5) {
		$('.expand-read-more').on('click', function() {
			$('.newsfeed-expand .news-item:hidden')
				.slice(0, 3)
				.show();
			if ($('.newsfeed-expand .news-item:hidden').length === 0) {
				$('.read-more').fadeOut(50);
			}
		});
	}
}

function emptyClass() {
	$('.panel-grid-cell').each(function() {
		if ($(this).html() == '&nbsp;') {
			$(this).addClass('empty');
		}
	});
}

$(function() {
	iframeResponsive();

	slideToggleFilter();

	hoverFilter();

	newsfeedToggle();

	if ($('body').hasClass('home') || $('body').hasClass('single')) {
		headerColor();
	}

	bottomAlignmentRight();

	bottomAlignmentLeft();

	lightboxInit();

	searchformInit();

	mobileNav();

	autoScroll();

	searchPlaceholder();

	// readmoreGrid()

	readmoreExpand();

	emptyClass();

	// module open -- !!!investigate
	// function moduleOpen() {
	//   $('.module-trigger').on('click', function(){
	//     var targetModule = $(this).attr('data-target')
	//     $('.module').each(function(){
	//       if ($(this).attr('id')==targetModule){
	//         $(this).addClass('visible')
	//         $('body').addClass('no-scroll')
	//       }
	//     })
	//   })
	// }

	// auto scroll -- !!!investigate
	// $("a.scroll-down").click(function(e) {
	//   e.preventDefault()
	//   var t = $(this).attr("href")
	//   $('html, body').animate({
	//       scrollTop: $(t).offset().top
	//   }, 500)
	// })
});
