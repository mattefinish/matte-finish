<?php 
/*
 * Template name: Reel
 */
get_header();
$hd_video_stream = get_field('hd_video_stream');
$sd_video_stream = get_field('sd_video_stream');
$lr_video_stream = get_field('lr_video_stream');
$local_mp4 = get_field('local_mp4');
$local_webm = get_field('local_webm');
$video_poster = get_field('video_poster');
$video_embed = get_field('reel_embed_code');
?>

<div class="video-wrap">
	<?php if(!preg_match('/(symbian|smartphone|phone|iphone|android|iPad)/i', strtolower($server))) : ?>
		<video id="matte-reel" class="background-video" autoplay loop="true" <?php if ( $video_poster ) : ?>poster="<?php echo $video_poster; ?>"<?php endif; ?> width="1280" height="720">
		    <?php if ( $hd_video_stream ) : ?><source src="<?php echo $hd_video_stream; ?>" type="video/mp4"><?php endif; ?>
		    <?php if ( $sd_video_stream ) : ?><source src="<?php echo $sd_video_stream; ?>" type="video/mp4"><?php endif; ?>
		    <?php if ( $lr_video_stream ) : ?><source src="<?php echo $lr_video_stream; ?>" type="video/mp4"><?php endif; ?>
		    <?php if ( $local_mp4 ) : ?><source src="<?php echo $local_mp4; ?>" type="video/mp4"><?php endif; ?>
		    <?php if ( $local_webm ) : ?><source src="<?php echo $local_webm; ?>" type="video/webm"><?php endif; ?>
		    <!--Your browser does not support the video tag. Please consider <a href="http://browsehappy.com/" target="_blank">upgrading</a> for a better experience. -->
		</video>
	<?php else: ?>
		<?php print_r($video_embed); ?>
	<?php endif; ?>
</div>

<!-- Video Controls -->

<div id="video-controls">
    <img id="play-pause" src="<?php echo get_template_directory_uri() . '/img/play.svg' ?>" />
    <img id="replay" src="<?php echo get_template_directory_uri() . '/img/restart.svg' ?>" />
    <img id="full-screen" src="<?php echo get_template_directory_uri() . '/img/full-screen.svg' ?>" />
</div>

<?php get_footer(); ?>