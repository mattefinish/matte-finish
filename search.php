<?php get_header(); ?>
<div class="content-wrap">
	<div class="back-wrapper">
		<a class="back" href="<?php bloginfo('url'); ?>">back</a>
	</div>
	<h1 class="taxonomy-title">
		<?php echo the_search_query(); ?>
	</h1>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
		<div class="post" style="background:url('<?php echo get_field('banner_image'); ?>') center center no-repeat; background-size:cover;">
		<a class="permalink" href="<?php the_permalink(); ?>"></a>
			<div class="inner-content">
				<div class="client-title"><?php wp_list_categories('title_li='); ?></div>
				<h1 class="post-title"><?php the_title(); ?></h1>
			</div>
			
		</div>
	
	<?php endwhile; endif; ?>
</div>

<?php get_footer(); ?>