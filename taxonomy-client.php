<?php 
get_header(); 
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
?>

<div class="content-wrap">
	<div class="back-wrapper">
		<a class="back" href="<?php bloginfo('url'); ?>">back</a>
	</div>
	<h1 class="taxonomy-title">
		<?php echo $term->name; ?>
	</h1>
	<?php if ( $term->description !== null ) : ?>
	<div class="taxonomy-description">
		<?php echo term_description(); ?>
	</div>
	<?php endif; ?>
	<div class="scroll-point">
		<?php
			if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			$linebreak_title = get_field('linebreak_title');
		?>
		<a class="permalink" href="<?php the_permalink(); ?>">
			<?php 
			$primary_banner = get_field('banner_image');
			$alt_banner = get_field('alternate_banner_image');
			if ( $primary_banner ) : 
			?>
				<div class="post" style="background:url('<?php echo $primary_banner; ?>') center center no-repeat; background-size:cover;">
			<?php else : ?>
				<div class="post" style="background:url('<?php echo $alt_banner; ?>') center center no-repeat; background-size:cover;">
			<?php endif; ?>
				<div class="inner-content">
					<div class="client-title"><?php echo $term->name; ?></div>
					<?php $banner_title = get_field('banner_title'); if(get_field('banner_title')): ?>
						<h1 class="narrow-post-title"><?php echo $banner_title; ?></h1>
					<?php else : ?>
						<h1 class="narrow-post-title"><?php the_title(); ?></h1>
					<?php endif; ?>
				</div>
			</div>
		</a>
		<?php endwhile; endif; ?>
	</div>
</div>

<?php get_footer(); ?>