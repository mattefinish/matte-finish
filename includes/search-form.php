<div class="search-background module">
	<div class="inner-wrap">
		<div class="inner-content module-content">
			<?php get_search_form(); ?>
			<ul class="categories">
				<li>or browse:</li>
				<?php wp_list_categories('title_li='); ?>
			</ul>
			<button class="close">back</button>
		</div>
	</div>
</div>