<?php

$clients = get_terms( 'client', array('hide_empty' => false) );

echo '<div class="content-wrap"><div class="inner-content"><ul>';

	foreach( $clients as $term ) : $client_link = get_field( 'client_link', $term ); $client_include = get_field('exclude_from_client_list', $term);

		// If not selected to exclude from the main client list

		if (!$client_include) :

			// If has a link to a client page

			if ( $client_link ) : 

				echo '<li><a href="' . get_term_link( $term->term_id ) . '">' . $term->name . '</a></li>';
			
			else : 

				echo '<li>' . $term->name . '</li>';

			endif; 

		endif;
	
	endforeach;

echo '</ul><img id="closeClients" src="' . get_template_directory_uri() . '/img/white-x.svg' . '" alt="Close"></div></div>';


?>