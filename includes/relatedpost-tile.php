<div class="news-item">
	<a class="news-link" href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail( 'square-size' , array( 'class' => 'news-thumbnail' ) ); ?>
		<h1 class="news-title"><?php the_title(); ?></h1>
	</a>
	<div class="news-categories">
		<?php the_category( ', ' ); ?>				
	</div>
</div>