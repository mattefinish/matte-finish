<?php
$description = get_field( 'description' );
$primary_banner = get_field( 'banner_image' );
$alt_banner = get_field( 'alternate_banner_image' );
?>
<div class="news-item">
	<div class="news-copy">
		<div class="left">
			<h1 class="news-title"><?php the_title(); ?></h1>
			<div class="news-categories">
				<?php the_category( ', ' ); ?>				
			</div>		
		</div>
		<div class="right">
			<?php if ($description) : ?>
			<div class="news-description"><?php echo $description; ?></div>
			<?php endif; ?>
			<a class="news-link" href="<?php the_permalink(); ?>">view project <span><i class="fa fa-long-arrow-right"></i></span></a>
		</div>
		<div class="clear"></div>	
	</div>
	<?php if ( $alt_banner ) : ?>
	<img src="<?php echo $alt_banner; ?>" class="news-thumbnail">
	<?php else : ?>
	<img src="<?php echo $primary_banner; ?>" class="news-thumbnail">
	<?php endif; ?>
</div>