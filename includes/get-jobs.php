<?php 

include_once 'careers/vendor/autoload.php';

use \Greenhouse\GreenhouseToolsPhp\GreenhouseService;
	
$harvestAPIkey =  'af243dae95b6c40045544f20be54403a-2';
$jobAPIkey =  'd2312a63a48acf950189f2b91efe3289-2';



/* Solution using job API -> cannot get the "employment_type" value */

/* 
$greenhouseService = new GreenhouseService([
	'apiKey' => $jobAPIkey, 
	'boardToken' => 'matteprojects'
]);

$jobApiService = $greenhouseService->getJobApiService();
$jobs = json_decode($jobApiService->getJobs(true));
$jobs = $jobs->jobs;
*/ 



/* Solution using Harvest API */

$greenhouseService = new GreenhouseService([
	'apiKey' => $harvestAPIkey, 
	'boardToken' => 'matteprojects'
]);

$harvestService = $greenhouseService->getHarvestService();

$jobs = json_decode($harvestService->getJobPosts());

foreach($jobs as $key => &$job_post){

	/*  Don't show if it's not live */
	if(!$job_post->live){
		unset($jobs[$key]);
		continue;
	}

	/*  Get the custom fields with 'employment_type' */
	$job = json_decode($harvestService->getJob(array('id'=>$job_post->job_id)));
	$job_post = (object) array_merge((array) $job_post, (array) $job->custom_fields);

	$job_post->content_long = htmlspecialchars_decode($job_post->content);
	
	$hr_pos = strpos($job_post->content_long , '<hr');

	$pattern = "/<p[^>]*>&nbsp;<\\/p[^>]*>/"; 

	if($hr_pos){
		$job_post->content_short =  substr($job_post->content_long, 0, $hr_pos);
		$job_post->content_long = substr($job_post->content_long,  $hr_pos+6);
		$job_post->content_long = preg_replace($pattern, '', $job_post->content_long); 
	} else {
		$job_post->content_short = $job_post->content_long;
		$job_post->content_long = false;
	}

	$job_post->content_short = preg_replace($pattern, '', $job_post->content_short); 


	/*  Generate the absolute url */
	$job_post->url = 'https://boards.greenhouse.io/matteprojects/jobs/'.$job_post->id;

}


