<div class="news-item">
	<a class="news-link" href="<?php the_permalink(); ?>">
		<?php 
			if ( has_post_thumbnail() ) :
				the_post_thumbnail( array( 800, 800 ), array( 'class' => 'news-thumbnail' ) );
			else :
				echo "<img src='". get_field( 'banner_image' ) . "' class='news-thumbnail'>";
			endif; 
		?>
		<h1 class="news-title"><?php the_title(); ?></h1>
	</a>
	<div class="news-categories">
		<?php the_category( ', ' ); ?>				
	</div>
</div>