<?php 

get_header(); 

if ( have_posts() ) : while ( have_posts() ) : the_post();

	echo '<div class="content-wrap page_content">';

		echo '<div class="inner-content">';

			echo '<h1>' . get_the_title() . '</h1>';

			the_content();

		echo '</div>';

	echo '</div>';

endwhile; endif; 
 
get_footer(); 
?>