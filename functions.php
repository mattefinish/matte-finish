<?php

function matte_setup() {

	// Let Wordpress Manage the Document Title
	add_theme_support( 'title-tag' );
	
	// hide user-facing admin bar when logged in
	show_admin_bar( false );

	// thumbnails support for blog posts
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'square-size', 600, 600, true );

};

add_action( 'after_setup_theme', 'matte_setup' );

// server var for functions echoing device-specific markup

$server = print_r( $_SERVER, true );

/*==========================================
ENQUEUE SCRIPTS AND STYLES
==========================================*/

function matte_scripts() {
	
	/*=== Wordpress Default Jquery ===*/
	
	if (!is_admin()) {
		wp_deregister_script( 'jquery' );
		wp_deregister_script('jquery-migrate');
    	wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), FALSE, NULL, TRUE);
		wp_register_script('jquery-migrate', includes_url('/js/jquery/jquery-migrate.min.js'), FALSE, NULL, TRUE);
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-migrate');
	}

	/*=== Font Awesome ===*/

	wp_enqueue_script('font-awesome', 'https://use.fontawesome.com/bfa5b116b0.js', '', '', true);

	/*=== Masonry JS ===*/

	wp_enqueue_script('masonry_plugin', get_template_directory_uri() . '/js/vendor/masonry/masonry.min.js', '', '', true);

	/*=== Plyr ===*/

	wp_enqueue_script('plyr-js', get_template_directory_uri() . '/js/vendor/plyr.min.js', '', '', true);

	wp_enqueue_style('pryl-css', get_template_directory_uri() . '/css/plyr.css', '', '', '');

	/*=== Images Loaded ===*/

	wp_enqueue_script('imagesLoaded_plugin', get_template_directory_uri() . '/js/vendor/masonry/imagesLoaded.min.js', '', '', true);

	/*=== FitVids===*/

	wp_enqueue_script('fitvids_plugin', get_template_directory_uri() . '/js/vendor/fitvids/fitvids.min.js', '', '', true);

	/*=== Lazysizes===*/

	wp_enqueue_script('lazysizes_plugin', get_template_directory_uri() . '/js/vendor/lazysizes.min.js', '', '', true);

	/*=== Flickity ===*/

	// wp_enqueue_script('flickity-js', get_template_directory_uri() . '/js/vendor/flickity.min.js', '', '', true);

	// wp_enqueue_script('flickity-fade-js', get_template_directory_uri() . '/js/vendor/flickity-fade.min.js', '', '', true);

	// wp_enqueue_style('flickity-css', get_template_directory_uri() . '/css/flickity.min.css', '', '', '');

	/*=== Main JS ===*/

	wp_enqueue_script('main_scripts', get_template_directory_uri() . '/js/main.js?v=1.0.3', '', '', true);

	/*=== Styles ===*/

	wp_enqueue_style('css-styles', get_template_directory_uri() . '/style.css?v=1.0.1', '', '', '');

	/*=== New Styles (Not done in Stylus) ===*/

	wp_enqueue_style('new-styles', get_template_directory_uri() . '/css/new.css?v=1.0.1', '', '', '');

}

add_action( 'wp_enqueue_scripts', 'matte_scripts' );


/*=============================================
YOAST
=============================================*/

/*=== Adjust Metabox Priority ===*/

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

/*==========================================
CUSTOM POST TYPES
==========================================*/

/*=== Project ===*/

function custom_post_type_project() {
	$labels = array(
		'name'                => ('Projects'),
		'singular_name'       => ('Project'),
		'menu_name'           => ('Projects'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Projects'),
		'view_item'           => ('View Project'),
		'add_new_item'        => ('Add New Project'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Project'),
		'update_item'         => ('Update Project'),
		'search_items'        => ('Search Projects'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('Project'),
		'description'         => ('Projects'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'page-attributes', 'excerpt' ),
		'hierarchical'        => false,
		'taxonomies'          => array( 'category', 'post_tag', 'client' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-format-gallery',
	);
	register_post_type( 'project', $args );
}
add_action( 'init', 'custom_post_type_project', 0 );

/*=== Careers ===*/

function custom_post_type_careers() {
	$labels = array(
		'name'                => ('Careers'),
		'singular_name'       => ('Career'),
		'menu_name'           => ('Careers'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Careers'),
		'view_item'           => ('View Career'),
		'add_new_item'        => ('Add New Career'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Career'),
		'update_item'         => ('Update Career'),
		'search_items'        => ('Search Careers'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('Career'),
		'description'         => ('Careers'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor'),
		'hierarchical'        => false,
		'taxonomies'          => array(),
		'public'              => true,
		'rewrite'             => array('slug' => 'career'),
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-id-alt',
	);
	register_post_type( 'matte-career', $args );
}
add_action( 'init', 'custom_post_type_careers', 0 );

/*==========================================
CUSTOM TAXONOMY 
==========================================*/

/*=== Client Taxonomy ===*/

function add_client_taxonomy() {
  register_taxonomy('client', array('post','project'), array(
      'hierarchical' => true,
      'labels' => array(
      'name' => ('Client'),
      'singular_name' => ('Client'),
      'search_items' =>  ('Search Clients' ),
      'all_items' => ('All Clients' ),
      'parent_item' => ('Parent Client' ),
      'parent_item_colon' => ('Parent Client:' ),
      'edit_item' => ('Edit Client' ),
      'update_item' => ('Update Client' ),
      'add_new_item' => ('Add New Client' ),
      'new_item_name' => ('New Client Name' ),
      'menu_name' => ('Clients' ),
    )
  ));
}

add_action( 'init', 'add_client_taxonomy', 0 );

/*==========================================
LIMIT POST REVISIONS
==========================================*/

function limit_post_revisions( $num, $post ) {
    $num = 3;
    return $num;
}
add_filter( 'wp_revisions_to_keep', 'limit_post_revisions', 10, 2 );

/*=============================================
CUSTOM LOGIN SCREEN
=============================================*/

// Change the login logo URL

function my_loginURL() {
    return 'http://mattefilms.com';
}
add_filter('login_headerurl', 'my_loginURL');

// Enqueue the login specific stylesheet for design customizations. CSS file is compiled through compass.

function my_logincustomCSSfile() {
    wp_enqueue_style('login-styles', get_template_directory_uri() . '/css/login.css');
}

add_action('login_enqueue_scripts', 'my_logincustomCSSfile');

/*================================= 
Page Link by Slug
=================================*/

function page_link_by_slug($slug) {

	// Get the post object of this page

    $page_object = get_page_by_path($slug);
 
	// Get the URL of this page
	
	$page_link = get_page_link( $page_object );

	return $page_link;
	
}

/*================================= 
Limit Image Upload Dimensions / Width
=================================*/

function matte_validate_image_size( $file ) {
	$image = getimagesize($file['tmp_name']);
	$maximum = array(
		'width' => '2000',
		'height' => '2000'
	);
	$image_width = $image[0];
	$image_height = $image[1];
	$too_large = "Image dimensions are too large. Maximum size is {$maximum['width']} by {$maximum['height']} pixels. Uploaded image is $image_width by $image_height pixels.";
	if ( $image_width > $maximum['width'] || $image_height > $maximum['height'] ) {
			//add in the field 'error' of the $file array the message
			$file['error'] = $too_large; 
			return $file;
	}
	else {
		return $file;
	}
}

add_filter('wp_handle_upload_prefilter','matte_validate_image_size');

/*================================= 
SiteOrigin
=================================*/

// Create a new template html for images so we can lazyload them.

function matte_sow_image_html( $template_html, $instance, $widget ){
	// Get the full image size.
	$full_image_src = wp_get_attachment_image_src($instance['image'], 'full')[0];
	$large_image_src = wp_get_attachment_image_src($instance['image'], 'large')[0];
	$medium_large_image_src = wp_get_attachment_image_src($instance['image'], 'medium_large')[0];
	$medium_image_src = wp_get_attachment_image_src($instance['image'], 'medium')[0];
	// Create a template string that has lazy loading, the image url and responsive sizes for the image.
	$new_template_html = '<div class="sow-image-container"><img style="max-width: 100%;" class="lazyload" data-src="' . $full_image_src . '" data-srcset="' . $medium_image_src . ' 300w, ' . $medium_large_image_src . ' 768w, ' . $large_image_src . ' 1000w, ' . $full_image_src . ' 2000w" /></div>';
  return $new_template_html;
}

add_filter('siteorigin_widgets_template_html_sow-image', 'matte_sow_image_html', 10, 3);

?>