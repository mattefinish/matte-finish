<?php 
get_header(); 
$featured_clients = get_field('featured_clients');
?>

<!-- Top area with hero slider. Shown on desktop -->

<div class="main-banner">
	<div class="inner-wrap">
		<div class="hide_on_mobile">	
			<div class="hero-slider-container">	
				<?php if( have_rows('project_slider') ): ?>
					<div class="hero-slider">
						<?php while( have_rows('project_slider') ): the_row(); ?>
							<a class="hero-slide <?php if(get_row_index() === 1): ?>hero-slide--is-selected<?php endif; ?>" href="<?php the_sub_field('link'); ?>">
								<img class="lazyload" data-src="<?php echo get_sub_field('image')['url']; ?>" />
							</a>
						<?php endwhile; ?>
					</div>
					<div class="hero-slider-nav">
						<?php while( have_rows('project_slider') ): the_row(); ?>
							<div class="hero-slider-nav__link <?php if(get_row_index() === 1): ?>hero-slider-nav__link--is-selected<?php endif; ?>">
								<div class="hero-slider-nav__content">
									<a class="hero-slider-nav__title" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a>
									<div class="hero-slider-nav__description">
										<div class="hero-slider-nav__description-title"><?php the_sub_field('description'); ?></div>
										<div class="hero-slider-nav__description-category"><?php the_sub_field('category'); ?></div>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="homepage_down_arrow">
				<a href="#scroll-point" class="scroll-down">
					<img src="<?php echo get_template_directory_uri() . '/img/white-down-arrow.svg'; ?>" />
				</a>
			</div>
		</div>
	</div>
</div>

<!-- Homepage About. Shown on mobile. -->

<div class="homepage_about_content">
	<h1><?php the_field('description', 7); ?></h1>
</div>

<!-- Newsfeed. Both Collapsed and Expanded Views -->

<div id="scroll-point" class="content-wrap scroll-point">

	<?php
		// Get the most recent post
		$recent_post_args = array('numberposts' => 1, 'orderby' => 'post_date', 'order' => 'DESC', 'post_type' => array('post', 'project'), 'post_status' => 'publish');
		$recent_post = wp_get_recent_posts( $recent_post_args );
		$recent_post_ID = $recent_post[0]['ID'];
		// Check it's featured image to get the width and height. The featured images orientation determines the grid configuration below.
		$recent_post_image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $recent_post_ID ), 'full' );
		$recent_post_image_width = $recent_post_image_data[1];
		$recent_post_image_height = $recent_post_image_data[2];
	?>

	<?php if($recent_post_image_width < $recent_post_image_height): ?>
		<div class="newsfeed-grid newsfeed-grid-configuration-1">
	<?php else: ?>
		<div class="newsfeed-grid newsfeed-grid-configuration-2">
	<?php endif; ?>
		<?php 
		// Newsfeed Grid. Shortcode via AJAX Load more plugin. Template stored in theme directory alm_templates
		echo do_shortcode('[ajax_load_more post_type="post, project" posts_per_page="6" theme_repeater="alm-newsfeed-grid.php" transition="fade" order="desc" orderby="date" scroll="false" button_label="more"]'); ?>
	</div>

</div>

<?php get_footer(); ?>