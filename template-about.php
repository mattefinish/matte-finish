<?php 
/*
 * Template name: About
 */
get_header(); 

if ( have_posts() ) : while ( have_posts() ) : the_post();

	echo '<div class="content-wrap">';

		echo '<div class="inner-content">';

			the_content();

		echo '</div>';

	echo '</div>';

endwhile; endif; 
 
get_footer(); 
?>