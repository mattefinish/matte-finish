<?php 
get_header();
if ( have_posts() ) : while ( have_posts() ) : the_post(); 
$banner_image = get_field('banner_image');
$alt_banner_image = get_field('alternate_banner_image');
if ( $alt_banner_image ) :
?>
	<div class="single-banner" style="background:url(<?php echo $alt_banner_image; ?>) center center no-repeat; background-size:cover;"></div>
<?php else: ?>
	<div class="single-banner" style="background:url(<?php echo $banner_image; ?>) center center no-repeat; background-size:cover;"></div>
<?php endif; ?>
<div class="content-wrap scroll-point">
	<?php the_content(); ?>
	<?php 
		/*=== Next and Back links on single posts ===*/

		// Pull clients associated with the post (should be one for each)
		$client = get_the_terms( $post->ID, 'client' );

		// If the post has a client
		if($client):

		// Save the client url
		$client_link = get_term_link($client[0]);
	?>
		<div class="post-nav">
			<div class="back-to-main">
				<a href="<?php echo $client_link; ?>">back to client</a>
			</div>
			<div class="next-project">
				<?php next_post_link('%link', 'next project'); ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php 
		else: 
	?>
		<div class="post-nav">
			<div class="back-to-main">
				<a href="<?php bloginfo('url'); ?>">back to main</a>
			</div>
			<div class="next-project">
				<?php next_post_link('%link', 'next project'); ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php endif; ?>

	<?php

		/*====================
		Related Articles Query
		====================*/

		$current_id = get_the_ID();

		// Create an array of the posts categories
		foreach((get_the_category()) as $category) {
		    $cat_slugs[] = $category->slug;
		}
         
		// The Client
		$client = get_the_terms( $post->ID, 'client' );

		// If the post has a client
		if($client):

		// Save the client name
		$client_slug = $client[0]->slug;

	?>

		<div class="related-articles">
			<div class="related-articles-title">related articles</div>
				<div class="newsfeed-grid">
					<?php 

					// Query for Posts/Projects with clients

				    $related_posts_args = array('post_type' => array('post', 'project'), 'orderby' => 'date', 'order' => 'DSC', 'posts_per_page' => 3, 'post__not_in' => array($current_id),
							'tax_query' => array(
											'relation' => 'OR',
											array(
												'taxonomy' => 'client',
												'field' => 'slug',
												'terms' => $client_slug
											),
											array(
												'taxonomy' => 'category',
												'field' => 'slug',
												'terms' => $cat_slugs
											)
										)
					);
				    $related_posts_loop = new WP_Query( $related_posts_args ); 
				    if ( $related_posts_loop->have_posts()): while ( $related_posts_loop->have_posts() ) : $related_posts_loop->the_post();
				    ?>

						<?php get_template_part('includes/relatedpost', 'tile'); ?>

					<?php endwhile; endif; wp_reset_postdata(); ?>
				</div>
			</div>
		</div>

	<?php else: ?>

		<div class="related-articles">
			<div class="related-articles-title">related articles</div>
				<div class="newsfeed-grid">
					<?php 

					// Query for Posts/Projects without clients

				    $related_posts_args = array('post_type' => array('post', 'project'), 'orderby' => 'date', 'order' => 'DSC', 'posts_per_page' => 3, 'post__not_in' => array($current_id),
							'tax_query' => array(
												'taxonomy' => 'category',
												'field' => 'slug',
												'terms' => $cat_slugs
											)
					);
				    $related_posts_loop = new WP_Query( $related_posts_args ); 
				    if ( $related_posts_loop->have_posts()): while ( $related_posts_loop->have_posts() ) : $related_posts_loop->the_post();
				    ?>

						<?php get_template_part('includes/relatedpost', 'tile'); ?>

					<?php endwhile; endif; wp_reset_postdata(); ?>
				</div>
			</div>
		</div>

	<?php endif; ?>

</div>
<?php 
endwhile; endif;
get_footer(); 
?>