        <footer id="footer">
            <div class="inner-wrap">
                <div class="footer_left">
                    <div class="address">
                        <?php the_field('footer_text', 2); ?>
                    </div>
                    <div class="social">
                        <a href="<?php the_field('facebook_link', 2); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="<?php the_field('instagram_link', 2); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                        <a href="<?php the_field('twitter_link', 2); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="<?php the_field('vimeo_link', 2); ?>" target="_blank"><i class="fa fa-vimeo"></i></a>
                    </div>
                </div>
                <div class="footer_right">
                    <img src="<?php echo get_template_directory_uri() . '/img/matte-logo-white.svg'; ?>" class="footer_logo" alt="Matte">
                    <ul class="matte_components">
                        <li><a href="<?php the_field('films_link', 2); ?>" target="_blank">Films</a></li>
                        <li><a href="<?php the_field('music_link', 2); ?>" target="_blank">Music</a></li>
                        <b><li><a href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank">Projects</a></li></b>
                    </ul>
                    <div class="email_subscribe">
                    <div id="mc_embed_signup">
                        <form action="//COM.us4.list-manage.com/subscribe/post?u=d60a686aa680084badf128c9d&amp;id=4003d5d709" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                                <div class="mc-field-group">
                                    <label for="mce-Email">Join Our Mailing List:</label>
                                    <input type="email" value="" name="EMAIL" class="required email" placeholder="" id="mce-EMAIL">
                                    <input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="button">
                                </div>
                                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_bfaeb9f9cfe94bf88ebe82105_58d7ecad1c" tabindex="-1" value=""></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </footer>
        <?php get_template_part('includes/search', 'form'); ?>
        <button id="searchform-trigger">search / filter</button>
        <a href="#" id="back-to-top">back to top <img src="<?php echo get_template_directory_uri() . '/img/arrow.svg' ?>" /></a>
        <?php wp_footer(); ?>
    </body>
</html>
