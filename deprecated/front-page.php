<?php 
get_header(); 
$featured_posts = get_field('featured_projects');
?>
<div class="main-banner">
	<div class="inner-wrap">
		<?php foreach( $featured_posts as $post ) : setup_postdata( $post ); ?>		
		<div id="post-<?php echo get_the_ID(); ?>" class="banner-image filtered-item" style="background:url(<?php echo get_field('banner_image'); ?>) center center no-repeat; background-size:cover;">
			<a data-id="post-<?php echo get_the_ID(); ?>" class="touch" href="<?php the_permalink(); ?>"><p><?php the_title(); ?></p></a>
		</div>
		<?php endforeach; wp_reset_postdata(); ?>	
		<div class="banner-title">
		<?php foreach( $featured_posts as $post ) : setup_postdata( $post ); ?>
			<a data-id="post-<?php echo get_the_ID(); ?>" class="filter" href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br />
		<?php endforeach; wp_reset_postdata(); ?>
		</div>
	</div>
</div>
<!-- need to figure out what to do with this link -->
<div class="recent-work"><a href="#">recent work</a></div>
<div class="content-wrap">
	<div class="newsfeed-toggle">
		<span>newsfeed</span>
		<span>:</span>
		<button id="expand">expand</button>
		<button id="collapse">collapse</button>
	</div>
<?php
get_template_part('includes/newsfeed', 'grid');
get_template_part('includes/newsfeed', 'expand');
?>
</div><!-- content-wrap end -->
<?php get_footer(); ?>