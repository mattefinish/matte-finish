<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="img/ico" href="<?php echo get_template_directory_uri() . '/favicon.ico'; ?>">
	<?php wp_head(); ?>

<script>
(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
e=o.createElement(i);r=o.getElementsByTagName(i)[0];
e.src='//www.google-analytics.com/analytics.js';
r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
ga('create','UA-79341802-1');ga('send','pageview');
</script>


    </head>
    <body <?php body_class(); ?>>

        <nav id="mobile-nav">
            <ul><?php wp_list_pages('exclude=2&title_li='); ?></ul>
        </nav>
        <?php $pages = get_posts( array( 'post_type' => 'page', 'exclude' => 2, 'order' => 'ASC' ) ); 
        if ( !is_page('about') && !is_page('clients') ) : foreach ( $pages as $post ) : setup_postdata( $post );
        if ( $post->post_name === 'about') : ?>
        <div id="post-<?php echo $post->ID; ?>" class="filtered-item slide-toggle">
            <div class="content-wrap">
                <div class="inner-content">
                    <?php echo the_content(); ?>
                </div>
            </div>
        </div>
        <?php elseif ( $post->post_name === 'clients') : ?>
        <div id="post-<?php echo $post->ID; ?>" class="filtered-item slide-toggle">
            <?php get_template_part('includes/clients', 'loop'); ?>
        </div>
        <?php endif; endforeach; wp_reset_postdata(); endif; ?>

        <!-- Mobile Navigation -->

        <div class="mobile_navigation">

            <div class="mobile_header">
                <div class="mobile_header_container">
                    <div class="mobile_logo_container"> 
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri() . '/img/matte-logo-black.svg'; ?>" class="black" alt="Matte"></a>
                    </div>
                    <div class="close_menu_icon"></div>
                </div>
            </div>
            
            <div class="content_container">
                <ul id="mobile-menu">
                    <li><a href="<?php echo get_page_link(7); ?>">About</a></li>
                    <li><a href="<?php echo get_page_link(9); ?>">Clients</a></li>
                    <li><a href="<?php echo get_page_link(11); ?>">Reel</a></li>
                    <li><a href="<?php echo page_link_by_slug('careers'); ?>">Careers</a></li>
                </ul>
                <div class="social">
                    <a href="<?php the_field('facebook_link', 2); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="<?php the_field('instagram_link', 2); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="<?php the_field('twitter_link', 2); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a href="<?php the_field('vimeo_link', 2); ?>" target="_blank"><i class="fa fa-vimeo"></i></a>
                    <a href="<?php the_field('email_link', 2); ?>" target="_blank"><i class="fa fa-envelope-o"></i></a>
                </div>
                <ul class="music_films_container">
                    <li><a href="<?php the_field('music_link', 2); ?>" target="_blank">Music</a></li>
                    <li><a href="<?php the_field('films_link', 2); ?>" target="_blank">Films</a></li>
                    <li><a href="<?php the_field('photography_link', 2); ?>" target="_blank">Events</a></li>
                 </ul>
            </div>  

        </div>

        <!-- Mobile Header -->

        <div class="main_mobile_header">

            <div class="mobile_header">
                <div class="mobile_header_container">
                    <div class="mobile_logo_container"> 
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri() . '/img/matte-logo-black.svg'; ?>" class="black" alt="Matte"></a>
                    </div>
                    <div class="menu_icon"></div>
                </div>
            </div>

        </div>

        <!-- Desktop Header -->

        <header id="header" <?php if ( is_archive() || is_search() ) : ?>class="black background true"<?php else : ?>class="<?php echo get_field('header_color'); ?>"<?php endif; ?>>
            <div class="inner-wrap">
                <div id="logo">
                    <a href="<?php bloginfo('url'); ?>">
                        <div class="inner-wrap">        
                            <img src="<?php echo get_template_directory_uri() . '/img/matte-logo-black.svg'; ?>" class="black" alt="Matte">                  
                            <img src="<?php echo get_template_directory_uri() . '/img/matte-logo-white.svg'; ?>" class="white" alt="Matte">
                        </div>
                    </a>
                    <div id="films-music">
                        <span class="music"><a href="<?php the_field('music_link', 2); ?>" target="_blank">Music</a></span>
                        <span class="films"><a href="<?php the_field('films_link', 2); ?>" target="_blank">Films</a></span>
                        <span><a href="<?php the_field('photography_link', 2); ?>" target="_blank">Events</a></span>
                    </div>
                </div>
                <nav id="main-nav">
                    <ul>
                        <li><a data-id="post-7" class="filter slide-toggle" style="cursor: default;">About</a></li>
                        <li><a data-id="post-9" class="filter slide-toggle" style="cursor: default;">Clients</a></li>
                        <li><a data-id="post-11" href="<?php echo get_page_link(11); ?>">Reel</a></li>
                        <li id="connect" >
                            <a href="#" class="connect_link">connect</a>
                            <ul class="social">
                                <li><a href="<?php the_field('facebook_link', 2); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="<?php the_field('instagram_link', 2); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="<?php the_field('twitter_link', 2); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="<?php the_field('vimeo_link', 2); ?>" target="_blank"><i class="fa fa-vimeo"></i></a></li>
                                <li><a href="<?php the_field('email_link', 2); ?>" target="_blank"><i class="fa fa-envelope-o"></i></a></li>
                                <li class="social_text_link"><a href="<?php echo page_link_by_slug('careers'); ?>">Careers</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>

            </div>
        </header>
