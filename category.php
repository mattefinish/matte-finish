<?php get_header(); ?>
<div class="content-wrap">
	<div class="back-wrapper">
		<a class="back" href="<?php bloginfo('url'); ?>">back</a>
	</div>
	<h1 class="taxonomy-title">
		<?php echo single_cat_title(); ?>
	</h1>
	<?php if ( category_description() !== "" ) : ?>
		<div class="taxonomy-description">
			<?php echo category_description(); ?>
		</div>
	<?php endif; ?>
	<div class="scroll-point">
		<?php 
			if ( have_posts() ) : while ( have_posts() ) : the_post();
			$primary_banner = get_field('banner_image');
			$alt_banner = get_field('alternate_banner_image'); 
		?>
		<a class="permalink" href="<?php the_permalink(); ?>">
			<?php if ( $primary_banner ) : ?>
			<div class="post" style="background:url('<?php echo $primary_banner; ?>') center center no-repeat; background-size:cover;">
			<?php else : ?>
			<div class="post" style="background:url('<?php echo $alt_banner; ?>') center center no-repeat; background-size:cover;">
			<?php endif; ?>	
				<div class="inner-content">
					<div class="client-title"><?php echo single_cat_title(); ?></div>
					<h1 class="post-title"><?php the_title(); ?></h1>
				</div>
			</div>
		</a>
		<?php endwhile; endif; ?>
	</div>
</div>

<?php get_footer(); ?>